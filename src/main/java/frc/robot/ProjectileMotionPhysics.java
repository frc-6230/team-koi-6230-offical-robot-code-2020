/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;


public class ProjectileMotionPhysics {

    public static final double EARTH_GRAVITY = 9.80665; // Newtons
    public static final double TOWER_HEIGHT = 2.47; // meters

    public static final double RPM_PERCENT = 1/RobotMap.CANNON_RPM;

    // returns the current amount for Victors output
    public static double omegaToVictorValue(double omega){
        return ((60 * omega)/(2*Math.PI))*RPM_PERCENT;
    }

    // returns the y coordinate according to x;
    public static double getY(double x,double v)
    {
        double y;
        y=RobotMap.CANNON_HEIGHT-((EARTH_GRAVITY/2)*(x/(v*Math.cos(RobotMap.CANNON_MOUNT_ANGLE*(Math.PI/180)))))+(Math.tan(RobotMap.CANNON_MOUNT_ANGLE*(Math.PI/180))*x);
        return y;
    }

    // returns the needed speed to shot
    public static double getSpeed(double x)
    {
        double y = TOWER_HEIGHT - RobotMap.CANNON_HEIGHT;
        double v;
        if(((x*Math.sin(Math.toRadians(2.0*RobotMap.CANNON_MOUNT_ANGLE)))-((2.0*y)*Math.pow(Math.cos(Math.toRadians(RobotMap.CANNON_MOUNT_ANGLE)), 2.0))) > 0)
        {
            v=Math.sqrt((Math.pow(x, 2.0)*EARTH_GRAVITY)/((x*Math.sin(Math.toRadians(2.0*RobotMap.CANNON_MOUNT_ANGLE)))-((2.0*y)*Math.pow(Math.cos(Math.toRadians(RobotMap.CANNON_MOUNT_ANGLE)), 2.0))));
            System.out.println(v);
            return v;
        }
        else{
            System.out.println("Can't shoot");
            return 0;

        }
    }

    public static double getVictorSpeed(double x){

        double offset = -0.0775*x + 0.7648;

        return omegaToVictorValue((getSpeed(x))/RobotMap.CANNON_WHEELS_RADIUS)+offset;
    }

}
