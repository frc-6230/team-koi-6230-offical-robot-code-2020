/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.ProjectileMotionPhysics;
import frc.robot.RobotMap;
import frc.robot.Vision;

public class ShootingSubsystem extends SubsystemBase {
  
  private VictorSP upperVictor, lowerVictor; // motors of the shooter

  public ShootingSubsystem() {
    upperVictor = new VictorSP(RobotMap.UPPER_SHOOTING_VICTOR);
    lowerVictor = new VictorSP(RobotMap.LOWER_SHOOTING_VICTOR);

    lowerVictor.setInverted(true);
  }

  // controlling manually
  public void shootByValue(double up, double down){
    upperVictor.set(up);
    lowerVictor.set(down);
  }

  public void shootByValue(double speed){
    upperVictor.set(speed);
    lowerVictor.set(speed);
  }

  public void autoShoot(){
    shootByValue(ProjectileMotionPhysics.getVictorSpeed(Vision.getDistanceFromReflective()));
  }



  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
