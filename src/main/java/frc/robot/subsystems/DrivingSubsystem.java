/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;
import frc.robot.Vision;
import frc.robot.koilib.SADlib.Driveable;
import frc.robot.koilib.SADlib.Path;
import frc.robot.koilib.SADlib.PathGroup;
import static frc.robot.Robot.turnPIDsettings;

public class DrivingSubsystem extends SubsystemBase implements Driveable{
  
  private VictorSP leftVictor, rightVictor; // Robot's driving motors
  private Encoder leftEnc, rightEnc; // Encoders
  private Gyro gyro; // gyro

  public DrivingSubsystem() {

    // Initizalzing the Victors
    leftVictor = new VictorSP(RobotMap.LEFT_DRIVING_VICTOR);
    rightVictor = new VictorSP(RobotMap.RIGHT_DRIVING_VICTOR);

    // Reversing one side.
    leftVictor.setInverted(true);

    leftEnc = new Encoder(RobotMap.LEFT_DRIVING_ENCODERS[0], RobotMap.LEFT_DRIVING_ENCODERS[1], true);
    rightEnc = new Encoder(RobotMap.RIGHT_DRIVING_ENCODERS[0], RobotMap.RIGHT_DRIVING_ENCODERS[1], false);

    gyro = new ADXRS450_Gyro();
  }

  // Stops the robot
  public void stop(){
    leftVictor.set(0);
    rightVictor.set(0);
  }

  // Driving method
  public void drive(double leftSpeed, double rightSpeed){
    leftVictor.set(leftSpeed);
    rightVictor.set(rightSpeed);
  }

 
  @Override
  public Gyro getGyro() {
    return gyro;
  }

  @Override
  public Encoder getLeftEncoder() {
    return leftEnc;
  }

  @Override
  public Encoder getRightEncoder() {
    return rightEnc;
  }

  public void aimToTower(){
    PathGroup pathGroup = new PathGroup(this, turnPIDsettings, turnPIDsettings);

    pathGroup.getPaths().add(new Path(0, Vision.getTurnAngleToInnerHole(gyro.getAngle())));

    pathGroup.excecute(this);
  }

  @Override
  public void periodic() {
    // drive()
  }
}
