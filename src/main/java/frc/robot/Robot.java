/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.koilib.SADlib.PID_Settings;
import frc.robot.subsystems.DrivingSubsystem;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {

  private Joystick js;
  public static DrivingSubsystem drivingSubsys;
  public static PID_Settings turnPIDsettings, drivePIDsettings;
  
 
  @Override
  public void robotInit() {
    js = new Joystick(0);

    drivingSubsys = new DrivingSubsystem();

    turnPIDsettings = new PID_Settings(0.03, 0, 0.01);
    turnPIDsettings.setAbsoluteTorelance(3);

    JoystickButton b = new JoystickButton(js, 0);
    
    b.whenPressed(drivingSubsys::aimToTower, drivingSubsys);

  }


  @Override
  public void robotPeriodic() {
 
    CommandScheduler.getInstance().run();
  }

 
  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
  }

 
  @Override
  public void autonomousInit() {
  }


  @Override
  public void autonomousPeriodic() {
  }

  @Override
  public void teleopInit() {

  }


  @Override
  public void teleopPeriodic() {
    // if(js.getRawButton(2)){
    //   System.out.println(js.getRawAxis(1));
    //   shootingSubsys.shootByValue(js.getRawAxis(1));
    // }else{
    //   shootingSubsys.shootByValue(0);
    // }
  }

  @Override
  public void testInit() {
    // Cancels all running commands at the start of test mode.
    CommandScheduler.getInstance().cancelAll();
  }

  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}
