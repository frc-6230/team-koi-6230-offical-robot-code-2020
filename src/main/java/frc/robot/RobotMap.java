/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * Add your docs here.
 */
public class RobotMap {

    public static final int LEFT_DRIVING_VICTOR = 0;
    public static final int RIGHT_DRIVING_VICTOR = 1;

    public static final int UPPER_SHOOTING_VICTOR = 0;
    public static final int LOWER_SHOOTING_VICTOR = 1;

    public static final int[] LEFT_DRIVING_ENCODERS = {0,1};
    public static final int[] RIGHT_DRIVING_ENCODERS = {2,3};

    public static final double LIMELIGHT_HEIGHT = 0.787; // meters
    public static final double LIMELIGHT_MOUNT_ANGLE = 32.5; // degrees
    public static final double DISTANCE_LIMELIGHT_TO_CANON = 0.24; // meters

    public static final double CANNON_MOUNT_ANGLE = 60; // degrees
    public static final double CANNON_HEIGHT = 1; // meters
    public static final double CANNON_WHEELS_RADIUS = 0.10; // meters
    public static final double CANNON_RPM = 18730/5; // rpm

}
