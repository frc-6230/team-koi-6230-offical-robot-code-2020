/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.koilib.Limelight.Limelight;

/**
 * Add your docs here.
 */
public class Vision {

    public static final double BOTTOM_REFLECTOR_HEIGHT = 2.06;
    public static final double DISTANCE_TO_INNER_HOLE = 0.75;

    public static double getDistanceFromReflective(){
        // (h2-h1)/tan(mount+result) + offset distance
        return (BOTTOM_REFLECTOR_HEIGHT - RobotMap.LIMELIGHT_HEIGHT)/Math.tan(Math.toRadians((Limelight.getTargetYAngle()+RobotMap.LIMELIGHT_MOUNT_ANGLE))) + RobotMap.DISTANCE_LIMELIGHT_TO_CANON;
    }

    // gets the length of the botto reflective line
    public static double getBottomLineLength(){
        double[] points = Limelight.getTargetPoints();

        return distance(points[4], points[5], points[6], points[7]);
    }

    // calculates  distance
    public static double distance(double x1, double x2, double y1, double y2){
        return Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2));
    }

    // calculates the requered angle to turn
    public static double getTurnAngleToInnerHole(double relativeAngle){
        double distance = getDistanceFromReflective();
        
        double angle = 90 - (relativeAngle + Limelight.getTargetXAngle());
        
        double xVector = distance * Math.cos(Math.toRadians(angle));
        double yVector = distance * Math.sin(Math.toRadians(angle));

        double beta = Math.toRadians(Math.atan2(yVector + DISTANCE_TO_INNER_HOLE, xVector));

        return relativeAngle - beta;
    }
    

}
