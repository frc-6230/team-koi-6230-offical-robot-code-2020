/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SMPlib;


import java.io.File;

import jaci.pathfinder.Pathfinder;
import jaci.pathfinder.Trajectory;
import jaci.pathfinder.Waypoint;

/**
 * Add your docs here.
 */
public class TrajectoryPath {

    private Trajectory trajectory;
    private Profile profile;

    public TrajectoryPath(Waypoint[] waypoints, Profile p){
       
        this.profile = p;
        
        Trajectory.Config config = new Trajectory.Config(Trajectory.FitMethod.HERMITE_CUBIC, Trajectory.Config.SAMPLES_HIGH, profile.getTimeStep(), profile.getVelocity(), profile.getAcceleration(), profile.getJerk());

        trajectory = Pathfinder.generate(waypoints, config);
    }

    public TrajectoryPath(String s, Profile p){
        this.profile = p;
        String path = "/home/lvuser/trajectories/" + s + ".csv";
        File myFile = new File(path);
        try{
            trajectory = Pathfinder.readFromCSV(myFile);
            System.out.println("Trajectory successfully loaded from " + path);
        }catch(Exception e){
            System.out.println("Error loading the trajectory at " + path);
        }
    }

    public void saveAs(String s){
        String path = "/home/lvuser/trajectories/" + s + ".csv";
        File myFile = new File(path);
        System.out.println(myFile);
        Pathfinder.writeToCSV(myFile, trajectory);

        System.out.println("The trajectory has been written successfully at " + path);
    }

    

    /**
     * @return the profile
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * @return the trajectory
     */
    public Trajectory getTrajectory() {
        return trajectory;
    }
    
}
