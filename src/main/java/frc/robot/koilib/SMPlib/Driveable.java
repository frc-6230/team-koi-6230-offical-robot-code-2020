/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SMPlib;

/**
 * Interface that represents the drivetrain
 */
public interface Driveable {

    // drive left method
    public void driveLeft(double leftSpeed);
    
    // drive right method
    public void driveRight(double rightSpeed);

    // Stop method
    public void stop();
    
}
