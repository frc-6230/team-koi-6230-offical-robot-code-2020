/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SMPlib;

/**
 * Add your docs here.
 */
public class Profile {
    private double p, i, d, velocity, acceleration;
    private double jerk, timeStep;
    private double robotWidth, wheelDiameter;
    private double leftTransmitorValue, rightTransmitorValue;
    private int leftEncTicks, rightEncTicks;

    public Profile(double robotWidth, double wheelDiameter, double timeStep){
        this.robotWidth = robotWidth;
        this.wheelDiameter = wheelDiameter;
        this.timeStep = timeStep;
    }

    public void setPIDVAJ(double p, double i, double d, double v, double a, double j){
        this.p = p;
        this.i = i;
        this.d = d;
        this.velocity = v;
        this.acceleration = a;
        this.jerk = j;
    }

    public void setLeftEncoder(int ticks, double trans){
        this.leftEncTicks = ticks;
        this.leftTransmitorValue = trans;
    }

    public void setRightEncoder(int ticks, double trans){
        this.rightEncTicks = ticks;
        this.rightTransmitorValue = trans;
    }

    /**
     * @return the leftEncTicks
     */
    public int getLeftEncTicks() {
        return leftEncTicks;
    }
    /**
     * @return the leftTransmitorValue
     */
    public double getLeftTransmitorValue() {
        return leftTransmitorValue;
    }
    /**
     * @return the rightEncTicks
     */
    public int getRightEncTicks() {
        return rightEncTicks;
    }

    /**
     * @return the rightTransmitorValue
     */
    public double getRightTransmitorValue() {
        return rightTransmitorValue;
    }

    /**
     * @param jerk the jerk to set
     */
    public void setJerk(double jerk) {
        this.jerk = jerk;
    }

    /**
     * @param robotWidth the robotWidth to set
     */
    public void setRobotWidth(double robotWidth) {
        this.robotWidth = robotWidth;
    }

    /**
     * @param timeStep the timeStep to set
     */
    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }

    /**
     * @param wheelDiameter the wheelDiameter to set
     */
    public void setWheelDiameter(double wheelDiameter) {
        this.wheelDiameter = wheelDiameter;
    }

    /**
     * @return the jerk
     */
    public double getJerk() {
        return jerk;
    }

    /**
     * @return the robotWidth
     */
    public double getRobotWidth() {
        return robotWidth;
    }

    /**
     * @return the timeStep
     */
    public double getTimeStep() {
        return timeStep;
    }

    /**
     * @return the wheelDiameter
     */
    public double getWheelDiameter() {
        return wheelDiameter;
    }

    /**
     * @return the acceleration
     */
    public double getAcceleration() {
        return acceleration;
    }

    /**
     * @return the d
     */
    public double getD() {
        return d;
    }

    /**
     * @return the i
     */
    public double getI() {
        return i;
    }

    /**
     * @return the p
     */
    public double getP() {
        return p;
    }

    /**
     * @return the velocity
     */
    public double getVelocity() {
        return velocity;
    }

    /**
     * @param acceleration the acceleration to set
     */
    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }
    /**
     * @param d the d to set
     */
    public void setD(double d) {
        this.d = d;
    }

    /**
     * @param i the i to set
     */
    public void setI(double i) {
        this.i = i;
    }

    /**
     * @param p the p to set
     */
    public void setP(double p) {
        this.p = p;
    }

    /**
     * @param velocity the velocity to set
     */
    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }
}
