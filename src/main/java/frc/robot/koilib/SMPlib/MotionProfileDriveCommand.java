/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SMPlib;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj2.command.CommandBase;
import jaci.pathfinder.Pathfinder;
import jaci.pathfinder.modifiers.TankModifier;

public class MotionProfileDriveCommand extends CommandBase {

  private Driveable driveable;
  private TrajectoryPath trajectoryPath;
  private Profile profile;
  private TransmitedEncoderFollower leftFollower, rightFollower;

  private Encoder leftEnc, rightEnc;
  private Gyro gyro;

  public MotionProfileDriveCommand(Driveable driveable, TrajectoryPath trajectoryPath, Encoder leftEnc, Encoder rightEnc, Gyro gyro) {
    this.driveable = driveable;
    
    this.trajectoryPath = trajectoryPath;
    
    this.profile = trajectoryPath.getProfile();

    this.leftEnc = leftEnc;
    this.rightEnc = rightEnc;
    this.gyro = gyro;


  }

  // Called just before this Command runs the first time
  @Override
  public void initialize() {

      TankModifier modifier = new TankModifier(trajectoryPath.getTrajectory()).modify(profile.getRobotWidth());

      leftFollower = new TransmitedEncoderFollower(modifier.getLeftTrajectory());
      rightFollower = new TransmitedEncoderFollower(modifier.getRightTrajectory());

      leftFollower.configureEncoder(leftEnc.get(), profile.getLeftEncTicks(),profile.getLeftTransmitorValue(), profile.getWheelDiameter());
      rightFollower.configureEncoder(rightEnc.get(), profile.getRightEncTicks(),profile.getRightTransmitorValue(), profile.getWheelDiameter());

      leftFollower.configurePIDVA(profile.getP(), profile.getI(), profile.getD(), 1 / profile.getVelocity(), 0); // P, I, D, 1 / Velocity, Acceleration
      rightFollower.configurePIDVA(profile.getP(), profile.getI(), profile.getD(), 1 / profile.getVelocity(), 0); // P, I, D, 1 / Velocity, Acceleration
      

    }

  // Called repeatedly when this Command is scheduled to run
  @Override
  public void execute() {
    
    double l = leftFollower.calculate(leftEnc.get()); // encoder_position_left
    double r = rightFollower.calculate(rightEnc.get()); // encoder_position_right

    
    
    double gyro_heading = gyro.getAngle();    // Assuming the gyro is giving a value in degrees 
    double desired_heading = Pathfinder.r2d(leftFollower.getHeading());  // Should also be in degrees
    
    // This allows the angle difference to respect 'wrapping', where 360 and 0 are the same value
    double angleDifference = Pathfinder.boundHalfDegrees(desired_heading - gyro_heading);
    angleDifference = angleDifference % 360.0;
    if (Math.abs(angleDifference) > 180.0) {
      angleDifference = (angleDifference > 0) ? angleDifference - 360 : angleDifference + 360;
    } 
    
    double turn = 0.8 * (-1.0/80.0) * angleDifference;

    driveable.driveLeft(l - turn);
    driveable.driveRight(r + turn);

  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  public boolean isFinished() {
    return leftFollower.isFinished() || rightFollower.isFinished();
  }

  // Called once after isFinished returns true
  @Override
  public void end(boolean interapted) {
    driveable.stop();
  }

}
