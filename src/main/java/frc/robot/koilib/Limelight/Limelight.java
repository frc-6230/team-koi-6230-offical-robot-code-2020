/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.Limelight;

import edu.wpi.first.networktables.NetworkTableInstance;

/**
 * Add your docs here.
 */
public class Limelight {
    public static final double kSTEAR = 0.3;
    public static final double kDRIVE = 0.3;
    // public static final double kMAX_AREA = 0.3;
    public static final double kMAX_SPEED = 0.3;


    public double[] getFollowerValuesSpeed(double maxArea){
        double[] speeds = {0,0};

        if(!hasTarget() || getTargetArea() >= maxArea)
            return speeds;

        double forward = (maxArea - getTargetArea()) * kDRIVE;
        double steer = getTargetXAngle() * kSTEAR;

        if(forward > kMAX_SPEED) forward = kMAX_SPEED;

        speeds[0] = forward + steer;
        speeds[1] = forward - steer;

        return speeds;
        
    }


    public static boolean hasTarget(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0) == 1;
    }

    public static double getTargetXAngle(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);
    }

    public static double getTargetYAngle(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(0);
    }
    
    public static double getTargetArea(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("ta").getDouble(0);
    }

    public static double getTargetWidth(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("thor").getDouble(0);
    }

    public static double getTargetHeight(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("tvert").getDouble(0);
    }

    public static double getTargetShortSide(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("tshort").getDouble(0);
    }

    public static double getTargetLongSide(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("tlong").getDouble(0);
    }

    public static void switchToDriverMode(){
        NetworkTableInstance.getDefault().getTable("limelight").getEntry("camMode").setNumber(1);
    }

    public static void switchToProccessingMode(){
        NetworkTableInstance.getDefault().getTable("limelight").getEntry("camMode").setNumber(0);
    }

    public static void setLedMode(int mode){
        NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode").setNumber(mode);
    }

    public static void setPipeline(int id){
        NetworkTableInstance.getDefault().getTable("limelight").getEntry("pipeline").setNumber(id);
    }

    public static double[] getTargetPoints(){
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("tcornxy").getDoubleArray(new double[8]);
    }


}
