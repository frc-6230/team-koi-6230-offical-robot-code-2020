package frc.robot.koilib.SADlib;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.interfaces.Gyro;

// simple interface to identify driving systems
public interface Driveable{

    // drive method
    public void drive(double leftSpeed, double rightSpeed);

    // stop method
    public void stop();

    public Encoder getLeftEncoder();
    public Encoder getRightEncoder();
    public Gyro getGyro();
    
}