/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SADlib;

/**
 * Add your docs here.
 */

 // A class that stores data about a certain path, distance and final turn
public class Path {

    private double distance; // meters
    private double turnAngle; // degrees

    
    public Path(double distance, double turnAngle){
        this.distance = distance;
        this.turnAngle = turnAngle;
    }

    public double getDistance(){
        return this.distance;
    }

    public double getAngle(){
        return this.turnAngle;
    }


    //reverses the path
    public void reverse(){
        distance = -distance;
        turnAngle = -turnAngle;
    }
}
