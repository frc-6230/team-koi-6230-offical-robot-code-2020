/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SADlib;

import java.util.LinkedList;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj2.command.SubsystemBase;



//Class that handles all the paths and makes the robot move according to them.
public class PathGroup {

    private LinkedList<Path> paths; // stores the paths

    private Driveable driveable; // the robot drivetrain

    private Encoder leftEncoder, rightEncoder; // encoders to measure distance

    private PID_Settings drivepidSettings; // strait drive pid settings 
    private PID_Settings turnpidSettings; // turn pid settings


    private Gyro gyro; // gyro

    // constructors:

    public PathGroup(){
        paths = new LinkedList<>();
    }

    public PathGroup(Driveable driveable, PID_Settings drivPid_Settings, PID_Settings turPid_Settings){
        paths = new LinkedList<>();

        this.driveable = driveable;

        this.leftEncoder = driveable.getLeftEncoder();
        this.rightEncoder = driveable.getRightEncoder();

        this.drivepidSettings = drivPid_Settings;
        this.turnpidSettings = turPid_Settings;

        this.gyro = driveable.getGyro();
    }

    public PathGroup(LinkedList<Path> p, Driveable driveable, PID_Settings drivPid_Settings, PID_Settings turPid_Settings){
        paths = p;

        this.driveable = driveable;

        this.leftEncoder = driveable.getLeftEncoder();
        this.rightEncoder = driveable.getRightEncoder();

        this.drivepidSettings = drivPid_Settings;
        this.turnpidSettings = turPid_Settings;

        this.gyro = driveable.getGyro();
    }

    public PathGroup(LinkedList<Path> p){
        paths = p;
    }

    public PathGroup(PathGroup pg){
        paths = pg.paths;

        this.driveable = pg.driveable;

        this.leftEncoder = pg.leftEncoder;
        this.rightEncoder = pg.rightEncoder;

        this.drivepidSettings = pg.drivepidSettings;
        this.turnpidSettings = pg.turnpidSettings;
        
        this.gyro = pg.gyro;
    }

    //end of constructors


    //method that calls the pid drive command, takes the drivetrain subsystem
    public void excecute(SubsystemBase subsystem){
        new PID_Drive(this, subsystem).schedule();
    }

    // reversing the paths
    public void reversePath(){
        LinkedList<Path> reversedList = new LinkedList<>();

        for(int i = paths.size() - 1; i >= 0; i--){
            Path p = paths.get(i);
            p.reverse();
            reversedList.add(p);
        }

        paths = reversedList;
    }

    /**
     * @param rightEncoder the rightEncoder to set
     */
    public void setRightEncoder(Encoder rightEncoder) {
        this.rightEncoder = rightEncoder;
    }

    /**
     * @param leftEncoder the leftEncoder to set
     */
    public void setLeftEncoder(Encoder leftEncoder) {
        this.leftEncoder = leftEncoder;
    }

    /**
     * @return the leftEncoder
     */
    public Encoder getLeftEncoder() {
        return leftEncoder;
    }

    /**
     * @return the rightEncoder
     */
    public Encoder getRightEncoder() {
        return rightEncoder;
    }

    /**
     * @param gyro the gyro to set
     */
    public void setGyro(ADXRS450_Gyro gyro) {
        this.gyro = gyro;
    }

    /**
     * @return the gyro
     */
    public Gyro getGyro() {
        return gyro;
    }

    /**
     * @return the paths
     */
    public LinkedList<Path> getPaths() {
        return paths;
    }


    /**
     * @return the driveable
     */
    public Driveable getDriveable() {
        return driveable;
    }

    /**
     * @param driveable the driveable to set
     */
    public void setDriveable(Driveable driveable) {
        this.driveable = driveable;
    }

    /**
     * @return the drivepidSettings
     */
    public PID_Settings getDrivepidSettings() {
        return drivepidSettings;
    }

    /**
     * @return the turnpidSettings
     */
    public PID_Settings getTurnpidSettings() {
        return turnpidSettings;
    }

    /**
     * @param drivepidSettings the drivepidSettings to set
     */
    public void setDrivepidSettings(PID_Settings drivepidSettings) {
        this.drivepidSettings = drivepidSettings;
    }

    /**
     * @param turnpidSettings the turnpidSettings to set
     */
    public void setTurnpidSettings(PID_Settings turnpidSettings) {
        this.turnpidSettings = turnpidSettings;
    }






}
