/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SADlib;


/**
 * Add your docs here.
 */
public class PID_Settings {

    //PID values
    private double kP;
    private double kI;
    private double kD;


    // The expected output range
    private double maxOutputRange;


    // How much the error can be ignored
    private double absoluteTorelance;

    // How much time the robot stay on the target
    private double finishWaitTime;


    // constructors

    public PID_Settings(double p, double i, double d){
        this(p,i,d,1,0, 0.5);
    }

    public PID_Settings(double p, double i, double d, double absoluteTorelance){
        this(p,i,d,1,absoluteTorelance, 0.5);
    }

    public PID_Settings(double p, double i, double d, double absoluteTorelance, double finishWaitTime){
        this(p,i,d,1,absoluteTorelance, finishWaitTime);
    }


    public PID_Settings(double p, double i, double d, double output, double absoluteTorelance, double finishWaitTime){

        kP = p;
        kI = i;
        kD = d;


        maxOutputRange = output;

        this.absoluteTorelance = absoluteTorelance;

        this.finishWaitTime = finishWaitTime;
    }


    // end of constructors


    /**
     * @return the kD
     */
    public double getkD() {
        return kD;
    }

    /**
     * @return the kI
     */
    public double getkI() {
        return kI;
    }

    /**
     * @return the kP
     */
    public double getkP() {
        return kP;
    }

    /**
     * @param kD the kD to set
     */
    public void setkD(double kD) {
        this.kD = kD;
    }

    /**
     * @param kI the kI to set
     */
    public void setkI(double kI) {
        this.kI = kI;
    }

    /**
     * @param kP the kP to set
     */
    public void setkP(double kP) {
        this.kP = kP;
    }

    /**
     * @return the absoluteTorelance
     */
    public double getAbsoluteTorelance() {
        return absoluteTorelance;
    }

    
    /**
     * @return the maxOutputRange
     */
    public double getMaxOutputRange() {
        return maxOutputRange;
    }


    /**
     * @param absoluteTorelance the absoluteTorelance to set
     */
    public void setAbsoluteTorelance(double absoluteTorelance) {
        this.absoluteTorelance = absoluteTorelance;
    }


    /**
     * @param maxOutputRange the maxOutputRange to set
     */
    public void setMaxOutputRange(double maxOutputRange) {
        this.maxOutputRange = maxOutputRange;
    }

    /**
     * @return the finishWaitTime
     */
    public double getFinishWaitTime() {
        return finishWaitTime;
    }

    /**
     * @param finishWaitTime the finishWaitTime to set
     */
    public void setFinishWaitTime(double finishWaitTime) {
        this.finishWaitTime = finishWaitTime;
    }
}
