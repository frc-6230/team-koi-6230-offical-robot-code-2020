/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.koilib.SADlib;

import edu.wpi.first.wpilibj.controller.*;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.Timer;

public class PID_Drive extends CommandBase {

  private PathGroup pathGroup; // all the paths

  private Driveable driveable; // drivetrain

  // pid settings
  private PID_Settings drivepidSettings;
  private PID_Settings turnpidSettings;

  // left and right speeds that the robot will drive
  private double leftSpeed, rightSpeed;

  // pid controllers that will drive the robot, left side, right side and turn in place
  private PIDController drivePIDright;
  private PIDController drivePIDleft;
  private PIDController turnPID;

  private double leftEncOffset, rightEncOffset, gyroOffset; // starting offsets of the sensors

  private double time = 0; // time counter to check the passed time

  private int pathID = 0; // curret path

  private int mode = 0; // the robot's mode. 0 - driving strait. 1 - turning in place

  //constructor
  public PID_Drive(PathGroup pg, SubsystemBase subsystem) {

    // disables other usages with drivetrain
    addRequirements(subsystem);
    
    //initiallizing
    pathGroup = pg;
    driveable = pathGroup.getDriveable();
    drivepidSettings = pathGroup.getDrivepidSettings();
    turnpidSettings = pathGroup.getTurnpidSettings();

    //setting the pid controllers
    drivePIDleft = new PIDController(drivepidSettings.getkP(), drivepidSettings.getkI(), drivepidSettings.getkD());




    drivePIDright = new PIDController(drivepidSettings.getkP() , drivepidSettings.getkI(), drivepidSettings.getkD());
    

    //0.007, 0.00003,0
    turnPID = new PIDController(turnpidSettings.getkP(), turnpidSettings.getkI(), turnpidSettings.getkD());

  }

  // initializing the pid controllers with the neccecery data
  @Override
  public void initialize() {
    //resetEncoders();

    //resetGyro();

    drivePIDleft.setSetpoint(pathGroup.getPaths().get(0).getDistance());
    drivePIDright.setSetpoint(pathGroup.getPaths().get(0).getDistance());

    drivePIDleft.setTolerance(drivepidSettings.getAbsoluteTorelance());
    drivePIDright.setTolerance(drivepidSettings.getAbsoluteTorelance());
  
    turnPID.setSetpoint(pathGroup.getPaths().get(0).getAngle());
    turnPID.setTolerance(turnpidSettings.getAbsoluteTorelance());

    leftEncOffset = pathGroup.getLeftEncoder().getDistance();
    rightEncOffset = pathGroup.getRightEncoder().getDistance();

    gyroOffset = pathGroup.getGyro().getAngle();

    time = Timer.getFPGATimestamp();


  }

  
  @Override
  public void execute() {

    // driving the robot
    driveable.drive(leftSpeed, rightSpeed);

    // mode check
    if (mode == 0) {

      // check if the robot on target with certain time
      if (isDriveOnTarget()) {
        if (Timer.getFPGATimestamp() - time >= drivepidSettings.getFinishWaitTime()) {

          // changing the mode to turn mode
          mode = 1;

          // handling and initialling
          stopMoving();

          //resetGyro();

          setTurnPath(pathID);

          updateTime();
        }
      } else {
        leftSpeed = drivePIDleft.calculate(pathGroup.getLeftEncoder().getDistance()) * drivepidSettings.getMaxOutputRange();
        rightSpeed = drivePIDleft.calculate(pathGroup.getRightEncoder().getDistance()) * drivepidSettings.getMaxOutputRange();
        updateTime();
      }

    } else if (mode == 1) {

      // check if the robot on target with certain time
      if (isTurnOnTarget()) {
        if (Timer.getFPGATimestamp() - time >= turnpidSettings.getFinishWaitTime()) {

          // changing the mode to drive mode
          mode = 0;

          // moving to the next path
          pathID++;

          // handling and initinalling
          stopMoving();

          if (pathID < pathGroup.getPaths().size()) {
            //resetEncoders();

            setDrivePath(pathID);

          }
          updateTime();
        }
      } else {
        leftSpeed = turnPID.calculate(pathGroup.getGyro().getAngle()) * turnpidSettings.getMaxOutputRange();
        rightSpeed = turnPID.calculate(pathGroup.getGyro().getAngle()) * turnpidSettings.getMaxOutputRange();
        updateTime();
      }
    }

  }

  // sets the drive distance (m)
  private void setDrivePath(int pathID){
    drivePIDleft.setSetpoint(pathGroup.getPaths().get(pathID).getDistance());
    drivePIDright.setSetpoint(pathGroup.getPaths().get(pathID).getDistance());
  }

  // sets the turn angle (d)
  private void setTurnPath(int pathID){
    turnPID.setSetpoint(pathGroup.getPaths().get(pathID).getAngle());
  }


  // return if driving is finished
  public boolean isDriveOnTarget(){
    double deltaLeft = pathGroup.getLeftEncoder().getDistance() - leftEncOffset;
    double deltaRight = pathGroup.getRightEncoder().getDistance() - rightEncOffset;

    boolean targetOnLeft = Math.abs(pathGroup.getPaths().get(pathID).getDistance() - deltaLeft) <= pathGroup.getDrivepidSettings().getAbsoluteTorelance();
    boolean targetOnRight= Math.abs(pathGroup.getPaths().get(pathID).getDistance() - deltaRight) <= pathGroup.getDrivepidSettings().getAbsoluteTorelance();

    return targetOnLeft || targetOnRight;
  }

  // returns if turing is finished
  public boolean isTurnOnTarget(){
    double deltaTurn = pathGroup.getGyro().getAngle() - gyroOffset;

    return Math.abs(pathGroup.getPaths().get(pathID).getAngle() - deltaTurn) <= pathGroup.getTurnpidSettings().getAbsoluteTorelance();
  }

  // updates the time
  private void updateTime(){
    time = Timer.getFPGATimestamp();

  }

  // resetting the encoders
  private void resetEncoders(){
    pathGroup.getLeftEncoder().reset();
    pathGroup.getRightEncoder().reset();
  }

  // resetting the gyro
  private void resetGyro(){
    pathGroup.getGyro().reset();
  }


  // stopping the robot from moving
  private void stopMoving(){
    leftSpeed = rightSpeed = 0;
    driveable.stop();
  }

  @Override
  public boolean isFinished() {

    return pathID >= pathGroup.getPaths().size();
  }

  @Override
  public void end(boolean interapted) {

    stopMoving();
  }


}
